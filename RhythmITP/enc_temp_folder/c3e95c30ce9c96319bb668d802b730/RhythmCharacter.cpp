// Fill out your copyright notice in the Description page of Project Settings.

#include "RhythmITP.h"
#include "RhythmCharacter.h"


// Sets default values
ARhythmCharacter::ARhythmCharacter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    moveComponent = CreateDefaultSubobject<UInterpToMovementComponent>(TEXT("OurMoveComponent"));
    moveComponent->UpdatedComponent = RootComponent;
    
}

// Called when the game starts or when spawned
void ARhythmCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARhythmCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ARhythmCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

