// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "Blueprint/UserWidget.h"
#include "RhythmITPGameMode.generated.h"

UCLASS(minimalapi)
class ARhythmITPGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ARhythmITPGameMode();
    
    UAudioComponent* PlaySong(class USoundWave* Sound);
    
    virtual void BeginPlay() override;
    
    /** Remove the current menu widget and create a new one from the specified class, if provided. */
    UFUNCTION(BlueprintCallable, Category = "UMG Game")
    void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

protected:
    /** The widget class we will use as our menu when the game starts. */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
    TSubclassOf<UUserWidget> MenuWidget;
    
    UPROPERTY()
    UUserWidget* CurrentWidget;
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundWave* Song;
    
    UPROPERTY(Transient)
    class UAudioComponent* songAC;
};



