// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "RhythmITP.h"
#include "RhythmITPGameMode.h"
#include "RhythmITPHud.h"
#include "RhythmCharacter.h"
#include "RhythmCharacterController.h"

ARhythmITPGameMode::ARhythmITPGameMode()
{
	DefaultPawnClass = ARhythmCharacter::StaticClass();
    PlayerControllerClass = ARhythmCharacterController::StaticClass();
	//HUDClass = ARhythmITPHud::StaticClass();
    
    // set default pawn class to our Blueprinted character

}

void ARhythmITPGameMode::BeginPlay()
{
    Super::BeginPlay();
    ChangeMenuWidget(MenuWidget);
}

void ARhythmITPGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
    if (CurrentWidget != nullptr)
    {
        CurrentWidget->RemoveFromViewport();
        CurrentWidget = nullptr;
    }
    if (NewWidgetClass != nullptr)
    {
        CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
        if (CurrentWidget != nullptr)
        {
            CurrentWidget->AddToViewport();
        }
    }
}
