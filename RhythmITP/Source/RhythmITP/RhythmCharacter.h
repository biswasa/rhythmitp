// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Blueprint/UserWidget.h"
#include "Components/InterpToMovementComponent.h"
#include "RhythmCharacterController.h"
#include "RhythmCharacter.generated.h"

UCLASS()
class RHYTHMITP_API ARhythmCharacter : public APawn
{
    GENERATED_BODY()
    
public:
    // Sets default values for this pawn's properties
    ARhythmCharacter();
    
    
    //test
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    bool getIsDropping()  { return isDropping;  }
    
    void playError();
    void addBeat(){totalBeats += 20;}
    
    // Quick fix for input
    bool leftPressed = false;
    bool middlePressed = false;
    bool rightPressed = false;
    
    // Called every frame
    virtual void Tick( float DeltaSeconds ) override;
    
    void Hit(int where);
    
    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
    
    void BoxTrace(USkeletalMeshComponent* box);
    
    UAudioComponent* PlaySong(class USoundCue* Sound);
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* Song;
    
    UPROPERTY(Transient)
    class UAudioComponent* songAC;
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* error;
    
    UPROPERTY(Transient)
    class UAudioComponent* errorAC;
    
    FTimerHandle SwitchTimer;
    
    void Switch();
    
    int32 GetScore();
    
    int32 GetTotalBeats();
    
    void addScore(int32 add);
    
    /** Remove the current menu widget and create a new one from the specified class, if provided. */
    UFUNCTION(BlueprintCallable, Category = "UMG Game")
    void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);
    
protected:
    /** The widget class we will use as our menu when the game starts. */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
    TSubclassOf<UUserWidget> UIWidget;
    
    UPROPERTY()
    UUserWidget* CurrentWidget;
    
    UPROPERTY(EditAnywhere, Category = Movement)
    UInterpToMovementComponent* moveComponent;
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
    USkeletalMeshComponent* BoxMesh;
    
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Score)
    int32 score;
    
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Score)
    int32 totalBeats;
    
    
    
    bool isDropping = true;
    
};
