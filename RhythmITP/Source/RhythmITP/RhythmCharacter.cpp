// Fill out your copyright notice in the Description page of Project Settings.

#include "RhythmITP.h"
#include "RhythmCharacter.h"
#include "Sound/SoundCue.h"
#include "BeatDrop.h"

// Sets default values
ARhythmCharacter::ARhythmCharacter()
{
    PrimaryActorTick.bCanEverTick = true;
    
    moveComponent = CreateDefaultSubobject<UInterpToMovementComponent>(TEXT("OurMoveComponent"));
    moveComponent->UpdatedComponent = RootComponent;
    
    score = 0;
    totalBeats = 0;
    
}

// Called when the game starts or when spawned
void ARhythmCharacter::BeginPlay()
{
    Super::BeginPlay();
    ChangeMenuWidget(UIWidget);
    songAC = PlaySong(Song);
    GetWorldTimerManager().SetTimer(SwitchTimer, this, &ARhythmCharacter::Switch, 55.876f, false);
    
}

void ARhythmCharacter::Switch(){
    isDropping = false;
    SetActorLocation(FVector(0,0,150));
    moveComponent->RestartMovement();
    songAC->Stop();
    songAC = PlaySong(Song);
}
// Called every frame
void ARhythmCharacter::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
}

// Called to bind functionality to input
void ARhythmCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);
    
}
UAudioComponent* ARhythmCharacter::PlaySong(USoundCue* Sound){
    UAudioComponent* AC = nullptr;
    if(Sound){
        AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
    }
    return AC;
}

void ARhythmCharacter::Hit(int where){
    static FName BeatClick = FName(TEXT("BeatTrace"));
    
    FVector StartPos = GetActorLocation();
    FVector actualStart;
    switch(where){
        case 0:
            actualStart = FVector(StartPos.X+120,StartPos.Y, StartPos.Z);
            break;
        case 1:
            actualStart = FVector(StartPos.X,StartPos.Y, StartPos.Z);
            break;
        case 2:
            actualStart = FVector(StartPos.X-120,StartPos.Y, StartPos.Z);
            break;
    }
    
    FVector Forward = GetActorForwardVector();
    // Calculate end position
    FVector EndPos = Forward * 200;
    // Perform trace to retrieve hit info
    FCollisionQueryParams TraceParams(BeatClick, true, Instigator);
    TraceParams.bTraceAsyncScene = true;
    TraceParams.bReturnPhysicalMaterial = true;
    // This fires the ray and checks against all objects w/ collision
    FHitResult Hit(ForceInit);
    GetWorld()->LineTraceSingleByObjectType(Hit, actualStart, EndPos,
                                            FCollisionObjectQueryParams::AllObjects, TraceParams);
    // Did this hit anything?
    if (Hit.bBlockingHit)
    {
        ABeatDrop* Beat = Cast<ABeatDrop>(Hit.GetActor());
        if (Beat)
        {
            Beat->SetHit();
            score += 10;
        } else {
            if (score > 0)
                score -= 1;
            //HUD->SubtractPoints(1);n
        }
    }
}

void ARhythmCharacter::playError(){
    errorAC = PlaySong(error);
}


void ARhythmCharacter::addScore(int32 add)
{
    //hacky way of playing error sound only when they miss
    score += add;
    //UE_LOG(LogTemp, Warning, TEXT("Score: %s"), score);
    if (score < 0) {
        score = 0;
    }
}
int32 ARhythmCharacter::GetScore()
{
    return score;
}

int32 ARhythmCharacter::GetTotalBeats()
{
    return totalBeats;
}


void ARhythmCharacter::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
    if (CurrentWidget != nullptr)
    {
        CurrentWidget->RemoveFromViewport();
        CurrentWidget = nullptr;
    }
    if (NewWidgetClass != nullptr)
    {
        CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
        if (CurrentWidget != nullptr)
        {
            CurrentWidget->AddToViewport();
        }
    }
}
