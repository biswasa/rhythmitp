// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "RhythmCharacter.h"
#include "GameFramework/Actor.h"
#include "BeatDrop.generated.h"

UCLASS()
class RHYTHMITP_API ABeatDrop : public AActor
{
  GENERATED_BODY()
  
public:
  // Sets default values for this actor's properties
  
  ABeatDrop();
  
  void setChar(ARhythmCharacter* op);
  
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;
  
  // Called every frame
  virtual void Tick( float DeltaSeconds ) override;
  
  bool InRange();
  
  void SetHit();
  
  void SetDirection(int dir) { myDirection = dir; };
  
protected:
  ARhythmCharacter* mainChar;
  
  // For beat tracking
  enum class Direction { Left = 0, Middle = 1, Right = 2 };
  int myDirection = -1;
  
  // Edit this to change buffer; consider making property
  float bufferLength = 50.0f;
  
  UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
  UStaticMeshComponent* NoteMesh;
};
