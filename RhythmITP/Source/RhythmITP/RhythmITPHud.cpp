// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "RhythmITP.h"
#include "RhythmITPHud.h"
#include "GameFramework/WheeledVehicle.h"
#include "RenderResource.h"
#include "Shader.h"
#include "Engine/Canvas.h"
#include "Vehicles/WheeledVehicleMovementComponent.h"
#include "Engine/Font.h"
#include "CanvasItem.h"
#include "Engine.h"
#include "RhythmCharacter.h"

// Needed for VR Headset
#if HMD_MODULE_INCLUDED
#include "IHeadMountedDisplay.h"
#endif // HMD_MODULE_INCLUDED 

#define LOCTEXT_NAMESPACE "VehicleHUD"

ARhythmITPHud::ARhythmITPHud()
{
	static ConstructorHelpers::FObjectFinder<UFont> Font(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	HUDFont = Font.Object;
}

void ARhythmITPHud::DrawHUD()
{
	Super::DrawHUD();

	// Calculate ratio from 720p
	const float HUDXRatio = Canvas->SizeX / 1280.f;
	const float HUDYRatio = Canvas->SizeY / 720.f;

	bool bWantHUD = true;
#if HMD_MODULE_INCLUDED
	if (GEngine->HMDDevice.IsValid() == true)
	{
		bWantHUD = GEngine->HMDDevice->IsStereoEnabled();
	}
#endif // HMD_MODULE_INCLUDED
	// We dont want the onscreen hud when using a HMD device	
	if (bWantHUD == true)
	{
		// Get our vehicle so we can check if we are in car. If we are we don't want onscreen HUD
		ARhythmCharacter* Box = Cast<ARhythmCharacter>(GetOwningPawn());
//		if ((Box != nullptr) && (Box->bInCarCameraActive == false))
//		{
//			FVector2D ScaleVec(HUDYRatio * 1.4f, HUDYRatio * 1.4f);
//
//			// Speed
//			FCanvasTextItem SpeedTextItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 455), Box->SpeedDisplayString, HUDFont, FLinearColor::White);
//			SpeedTextItem.Scale = ScaleVec;
//			Canvas->DrawItem(SpeedTextItem);
//
//			// Gear
//			FCanvasTextItem GearTextItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 500.f), Vehicle->GearDisplayString, HUDFont, Vehicle->bInReverseGear == false ? Vehicle->GearDisplayColor : Vehicle->GearDisplayReverseColor);
//			GearTextItem.Scale = ScaleVec;
//			Canvas->DrawItem(GearTextItem);
//		}
	}
}


#undef LOCTEXT_NAMESPACE
