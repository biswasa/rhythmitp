// Fill out your copyright notice in the Description page of Project Settings.

#include "RhythmITP.h"
#include "BeatDrop.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"

// Sets default values
void ABeatDrop::setChar(ARhythmCharacter* op)
{
    mainChar = op;
}

ABeatDrop::ABeatDrop()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    
    NoteMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("NoteMesh"));
    RootComponent = NoteMesh;
    
    
    
}

// Called when the game starts or when spawned
void ABeatDrop::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void ABeatDrop::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    if(mainChar != nullptr && !mainChar->getIsDropping()){
        FVector loc = mainChar->GetActorLocation();
        FVector ourLoc = GetActorLocation();
        
        if(loc.Y-370 > ourLoc.Y) { // Missed
            mainChar->addScore(-5);
            mainChar->playError();
            Destroy();
        }
        
        if (InRange()) { // Check if hit within margin of error
            switch (myDirection) {
                case 0:
                    if (mainChar->leftPressed) {
                        mainChar->addScore(20);
                        mainChar->leftPressed = false;
                        Destroy();
                    }
                    break;
                case 1:
                    if (mainChar->middlePressed) {
                        mainChar->addScore(20);
                        mainChar->middlePressed = false;
                        Destroy();
                    }
                    break;
                case 2:
                    if (mainChar->rightPressed) {
                        mainChar->addScore(20);
                        mainChar->rightPressed = false;
                        Destroy();
                    }
                    break;
                default:
                    // Do nothing
                    break;
            }
        }
        
    }
}

bool ABeatDrop::InRange() {
    FVector loc = mainChar->GetActorLocation();
    FVector ourLoc = GetActorLocation();
    
    return FMath::IsWithin<float>(ourLoc.Y, loc.Y - bufferLength, loc.Y + bufferLength);
}

void ABeatDrop::SetHit(){
    Destroy();
}
