// Fill out your copyright notice in the Description page of Project Settings.

#include "RhythmITP.h"
#include "RhythmCharacter.h"
#include "RhythmCharacterController.h"
#include "BeatDrop.h"



ARhythmCharacterController::ARhythmCharacterController()
{
    bShowMouseCursor = true;
    DefaultMouseCursor = EMouseCursor::Crosshairs;
    static ConstructorHelpers::FObjectFinder<UBlueprint> RedBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_Note_Red.BP_Note_Red'"));
    static ConstructorHelpers::FObjectFinder<UBlueprint> BlueBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_Note_Blue.BP_Note_Blue'"));
    static ConstructorHelpers::FObjectFinder<UBlueprint> GreenBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_Note_Green.BP_Note_Green'"));
    if (RedBlueprint.Object){
        MyRedBlueprint = (UClass*)RedBlueprint.Object->GeneratedClass;
    }
    if (BlueBlueprint.Object){
        MyBlueBlueprint = (UClass*)BlueBlueprint.Object->GeneratedClass;
    }
    if (GreenBlueprint.Object){
        MyGreenBlueprint = (UClass*)GreenBlueprint.Object->GeneratedClass;
    }
    
    
}

void ARhythmCharacterController::PlayerTick(float DeltaTime)
{
    Super::PlayerTick(DeltaTime);
}

void ARhythmCharacterController::clearLeft(){
    ARhythmCharacter* pawn = Cast<ARhythmCharacter>(GetPawn());
    
    if (pawn != nullptr)
    {
        pawn->leftPressed = false;
    }
    
}

void ARhythmCharacterController::clearRight(){
    ARhythmCharacter* pawn = Cast<ARhythmCharacter>(GetPawn());
    
    if (pawn != nullptr)
    {
        pawn->rightPressed = false;
    }
    
}

void ARhythmCharacterController::clearMiddle(){
    ARhythmCharacter* pawn = Cast<ARhythmCharacter>(GetPawn());
    
    if (pawn != nullptr)
    {
        pawn->middlePressed = false;
    }
    
}
void ARhythmCharacterController::Left()
{
    ARhythmCharacter* pawn = Cast<ARhythmCharacter>(GetPawn());
    
    if (pawn != nullptr)
    {
        FVector loc = pawn->GetActorLocation();
        FRotator rot = pawn->GetActorRotation();
        FVector temp = FVector(loc.X+120, loc.Y, loc.Z-offset);
        if(pawn->getIsDropping()){
            ABeatDrop* beat = (ABeatDrop*) GetWorld()->SpawnActor<ABeatDrop>(MyRedBlueprint, temp, rot);
            beat->setChar(pawn);
            beat->SetDirection(0);
            pawn->addBeat();
        }else{
            //pawn->Hit(0);
            pawn->leftPressed = true;
            GetWorldTimerManager().ClearTimer(SwitchTimer);
            GetWorldTimerManager().SetTimer(SwitchTimer, this, &ARhythmCharacterController::clearLeft, 0.5f, false);
        }
    }
}

void ARhythmCharacterController::Middle()
{
    
    ARhythmCharacter* pawn = Cast<ARhythmCharacter>(GetPawn());
    
    
    if (pawn != nullptr)
    {
        FVector loc = pawn->GetActorLocation();
        FRotator rot = pawn->GetActorRotation();
        FVector temp = FVector(loc.X, loc.Y, loc.Z-offset);
        if(pawn->getIsDropping()){
            ABeatDrop* beat = (ABeatDrop*) GetWorld()->SpawnActor<ABeatDrop>(MyBlueBlueprint, temp, rot);
            beat->setChar(pawn);
            beat->SetDirection(1);
            pawn->addBeat();
        }else{
            // pawn->Hit(1);
            pawn->middlePressed = true;
            GetWorldTimerManager().ClearTimer(SwitchTimer);
            GetWorldTimerManager().SetTimer(SwitchTimer, this, &ARhythmCharacterController::clearMiddle, 0.5f, false);
        }
    }
    
}
void ARhythmCharacterController::Right()
{
    
    ARhythmCharacter* pawn = Cast<ARhythmCharacter>(GetPawn());
    
    
    if (pawn != nullptr)
    {
        FVector loc = pawn->GetActorLocation();
        FRotator rot = pawn->GetActorRotation();
        FVector temp = FVector(loc.X-120, loc.Y, loc.Z-offset);
        if(pawn->getIsDropping()){
            ABeatDrop* beat = (ABeatDrop*) GetWorld()->SpawnActor<ABeatDrop>(MyGreenBlueprint, temp, rot);
            beat->setChar(pawn);
            beat->SetDirection(2);
            pawn->addBeat();
        }else{
            // pawn->Hit(2);
            
            pawn->rightPressed = true;
            GetWorldTimerManager().ClearTimer(SwitchTimer);
            GetWorldTimerManager().SetTimer(SwitchTimer, this, &ARhythmCharacterController::clearRight, 0.5f, false);
            
        }
    }
    
}

void ARhythmCharacterController::Rotate180(){
    APawn* pawn = GetPawn();
    FRotator rot = pawn->GetActorRotation();
    FRotator newRotation = FRotator(rot.Pitch,rot.Yaw+180, rot.Roll);
    pawn->SetActorRotation(newRotation);
}

void ARhythmCharacterController::SetupInputComponent()
{
    // set up gameplay key bindings
    Super::SetupInputComponent();
    InputComponent->BindAction("Left", IE_Pressed, this, &ARhythmCharacterController::Left);
    InputComponent->BindAction("Middle", IE_Pressed, this, &ARhythmCharacterController::Middle);
    InputComponent->BindAction("Right", IE_Pressed, this, &ARhythmCharacterController::Right);
    InputComponent->BindAction("Rotation", IE_Pressed, this, &ARhythmCharacterController::Rotate180);
}
