// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Controller.h"
#include "RhythmCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class RHYTHMITP_API ARhythmCharacterController : public APlayerController
{
	GENERATED_BODY()
public:
    ARhythmCharacterController();
    
private:
    /** True if the controlled character should navigate to the mouse cursor. */
    uint32 bMoveToMouseCursor : 1;
    
    // Begin PlayerController interface
    virtual void PlayerTick(float DeltaTime) override;
    virtual void SetupInputComponent() override;
    // End PlayerController interface
    TSubclassOf<class ABeatDrop> MyRedBlueprint;
    TSubclassOf<class ABeatDrop> MyBlueBlueprint;
    TSubclassOf<class ABeatDrop> MyGreenBlueprint;
    int offset = 130;

    
    /** Input handlers for SetDestination action. */
    void Left();
    void Middle();
    void Right();
    void Rotate180();
    void clearLeft();
    void clearRight();
    void clearMiddle();
    FTimerHandle SwitchTimer;

};
